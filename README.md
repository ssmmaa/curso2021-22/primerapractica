# Práctica a realizar

En esta primera práctica será una primera toma de contacto con la creación de agentes y una comunicación básica entre ellos para alcanzar las tareas que se pedirán realizar.

Los agentes y sus tareas serán los siguientes:

- `AgenteConsola` : Presentará una ventana asociada a cada uno de los agentes que le envíe mensajes que deban ser presentados en una ventana exclusiva del agente que le ha enviado el mensaje. Sus tareas serán:
	- *Recibir mensajes*: Tarea cíclica que espera un mensaje que se almacenará en una estructura para añadirlo a la ventana correspondiente del agente que lo envió.
	- *Presentar mensaje*: Será una tarea de una sola ejecución por cada mensaje que se quiera presentar en la ventana del agente que envió el mensaje. Si no está previamente creada se creará una nueva.

- `AgenteComprador` : Este agente representa a un comprador que está buscando productos según categorías y hasta gastar el dinero que tiene disponible. Las categorías de productos que buscará y el dinero disponible para la compra serán definidos por el usuario antes de que el agente realice sus tareas. Las tareas que deberá completar son las siguientes:
	- Localizar a los agentes que venden productos relacionados con las categorías en las que está interesado.
	- Realizar una oferta para la compra de un producto a los agentes que ha localizado y mientas tenga dinero disponible.
	- Comprobar las ofertas que han sido aceptadas para no exceder la cantidad de dinero disponible para la adquisición de productos.
	- Enviar al `AgenteConsola` un registro de la actividad que está realizando en el cumplimiento de sus tareas.

- `AgenteVendedor` : Este agente representa a un vendedor de una o varias categorías de productos. El usuario deberá definir las categorías y productos de cada categoría disponibles de este agente antes de comenzar con sus tareas. Las tareas que deberá realizar son las siguientes:
	- Se registrará según las categorías de productos que venda.
	- Recibirá ofertas de clientes durante un tiempo establecido.
	- Resolver las ofertas que se han recibido y comunicar el resultado a los agentes implicados.
	- Enviar al `AgenteConsola` un registro de la actividad que está realizando en el cumplimiento de sus tareas.

Para la prueba de los agentes, salvo el `AgenteConsola`, se deberán crear tres múltiples instancias que demuestren el funcionamiento de todos los agentes en conjunto.

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTgzMjc1MTYzOV19
-->